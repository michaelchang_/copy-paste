package mchang31415.copy_paste;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import android.os.AsyncTask;

import org.json.JSONObject;

public class TestNetwork extends AsyncTask<String, String, String> {
    private static final String TAG = TestNetwork.class.getSimpleName();

    private void printList(List<String> l, String t) {
        for(String s : l)
            Log.d(t, s);
    }


    @Override
    protected String doInBackground(String... urls) {
        String key = "b5e5cOdcgntvD1a-98mP2CheVc4Qx_iU";
        String database = "copy-paste", collection = "copy";
        mlabAPIWrapper mlabAPI = new mlabAPIWrapper(key);

        mlabEntry mostRecent = mlabAPI.getMostRecentDocument(database, collection);

        mlabEntry newEntry = new mlabEntry(mostRecent.getObjectId(), "ecksdee");
        String response = mlabAPI.insertDocument(database, collection, newEntry);
        Log.d(TAG, response);
        return "";
    }

}
