package mchang31415.copy_paste;

import com.google.gson.annotations.SerializedName;

public class GsonCompatibleObjectId {
    @SerializedName("$oid")
    private String m_id;
    public GsonCompatibleObjectId(String id) {
        m_id = id;
    }
    public GsonCompatibleObjectId() { m_id = ""; }
    public String getId() { return m_id; }
    public void setId(String id) { m_id = id; }
}
