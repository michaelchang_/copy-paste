package mchang31415.copy_paste;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class mlabEntry implements Constants {
    private static final String TAG = mlabEntry.class.getSimpleName();

    private GsonCompatibleObjectId _id;
    private GsonCompatibleObjectId parentId;
    private String value;
    private GsonCompatibleDate created;

    mlabEntry() {
        _id = parentId = null;
        value = "";
        created = null;
    }

    mlabEntry(String _id, String parentId, String value, Date created) {
        if (_id != null)
            this._id = new GsonCompatibleObjectId(_id);
        if (parentId != null)
            this.parentId = new GsonCompatibleObjectId(parentId);
        this.value = value;
        if (created != null)
            this.created = new GsonCompatibleDate(created);
    }

    mlabEntry(String parentId, String value, Date created) {
        this(null, parentId, value, created);
    }
    mlabEntry(String parentId, String value) { this(parentId, value, new Date()); }

    String getObjectId() {
        if (this._id == null) return "";
        return _id.getId();
    }
    void setObjectId(String _id) {
        if (this._id == null) this._id = new GsonCompatibleObjectId(_id);
        else this._id.setId(_id);
    }

    String getParentId() {
        if (this.parentId == null) return "";
        return this.parentId.getId();
    }

    void setParentId(String parentId) {
        if (this.parentId == null) this.parentId = new GsonCompatibleObjectId(parentId);
        else this.parentId.setId(parentId);
    }

    Date getTimestamp() {
        if (this.created == null) return new Date();
        return created.getDate();
    }
    void setTimestamp() {
        if (this.created == null) this.created = new GsonCompatibleDate(new Date());
        this.created.setDate(new Date());
    }

    String getValue() {
        if (this.value == null) return "";
        return this.value;
    }
    void setValue(String value) {
        this.value = value;
    }

    static String stringify(Map<String, String> map) {
        String result = "{";
        boolean appendFlag = false;
        for(Map.Entry<String, String> entry : map.entrySet()) {
            if (appendFlag) result += ", ";
            switch (entry.getKey()) {
                case "_id":
                case "parentId":
                    result += String.format("\"%s\": {\"$oid\": \"%s\"}", entry.getKey(), entry.getValue());
                    break;
                case "value":
                    result += String.format("\"%s\": \"%s\"", entry.getKey(), entry.getValue());
                    break;
                case "created":
                    result += String.format("\"%s\": {\"$date\": \"%s\"}", entry.getKey(), entry.getValue());
                    break;
                default:
                    Log.d(TAG, "stringify() Illegal argument provided " + entry.getKey() + ": " + entry.getValue());
                    return "";
            }
            appendFlag = true;
        }
        result += "}";
        return result;
    }

    String stringify() { return stringify(false); }

    String stringify(boolean includeObjectIdFlag) {
        Map<String, String> map = new HashMap<>();
        if (includeObjectIdFlag && _id != null) map.put("_id", getObjectId());
        if (parentId != null) map.put("parentId", getParentId());
        if (!value.isEmpty()) map.put("value", value);
        if (created != null) map.put("created", dateFormat.format(created.getDate()));
        return stringify(map);
    }
}
