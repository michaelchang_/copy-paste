package mchang31415.copy_paste;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;

import android.util.Log;
import java.util.Map;
import java.util.HashMap;
import java.io.Closeable;

public class MainActivity extends AppCompatActivity
                          implements Constants
{
    private static final String TAG = MainActivity.class.getSimpleName();

    private Map<String, String> m_mongoConfigMap = new HashMap<>();

    public static boolean cleanupStream(Closeable stream, String printOnFailure) {
        try {
            stream.close();
            return true;
        }
        catch(Exception e) {
            //if (DEBUG) Log.d(TAG, printOnFailure, e);
            if (DEBUG) Log.d(TAG, printOnFailure);
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (doesFileExist(mongoConfigFilename)) {
            FileInputStream fis = null;
            BufferedReader reader = null;
            try {
                fis = new FileInputStream(mongoConfigFilename);
                reader = new BufferedReader(new InputStreamReader(fis));
                String line = reader.readLine();
                while (line != null) {
                    String[] parts = line.split(":");
                    m_mongoConfigMap.put(parts[0], parts[1]);
                }

                startServiceWithMongoConfig();

            } catch (FileNotFoundException e){
                if (DEBUG) Log.d(TAG, "onCreate() Config file not found.");
            } catch (IOException e) {
                //found the file but can't read from it
                if (DEBUG) Log.d(TAG, "onCreate() Error reading from config file.", e);
            } finally {
                cleanupStream(reader, "onCreate() Reader close failure.");
                cleanupStream(fis, "onCreate() FileInputStream close failure");
            }

        }


    }

    void startServiceWithMongoConfig() {
        Intent intent = new Intent(this, MyService.class);
        intent.putExtra(DATABASE_NAME, m_mongoConfigMap.get(DATABASE_NAME));
        intent.putExtra(COLLECTION_NAME, m_mongoConfigMap.get(COLLECTION_NAME));
        intent.putExtra(API_KEY, m_mongoConfigMap.get(API_KEY));
        startService(intent);
    }

    private boolean doesFileExist(String fname) {
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }
    private String getStringById(int id) {
        EditText editText = (EditText) findViewById(id);
        return editText.getText().toString().trim();
    }

    private void writeToConfig(){
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(mongoConfigFilename, Context.MODE_PRIVATE);
            for (Map.Entry<String, String> entry : m_mongoConfigMap.entrySet()) {
                String s = entry.getKey() + ":" + entry.getValue();
                fos.write(s.getBytes());
                fos.write(System.getProperty("line.separator").getBytes());
            }
            fos.flush();
            fos.close();
            startServiceWithMongoConfig();
        } catch (IOException e) {
            if (DEBUG) Log.d(TAG, "doneButtonOnClick(View v) Cannot write to config file.", e);
        } finally {
            cleanupStream(fos, "doneButtonOnClick(View v) FileOutputStream cleanup.");
        }
    }

    public void doneButtonOnClick(View v) {

        //new TestNetwork().execute();

        //If the file already exists, delete it and put in new config.
        if (doesFileExist(mongoConfigFilename)) {
            deleteFile(mongoConfigFilename);
        }
        m_mongoConfigMap.put(DATABASE_NAME, getStringById(R.id.databaseName));
        m_mongoConfigMap.put(COLLECTION_NAME, getStringById(R.id.collectionName));
        m_mongoConfigMap.put(API_KEY, getStringById(R.id.apiKey));
        writeToConfig();
        try {
            startServiceWithMongoConfig();
        } catch (Exception e) {
            Log.d("done button", "failed to connect", e);
        }
    }

} // end MainActivity
