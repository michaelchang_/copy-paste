package mchang31415.copy_paste;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CollectionStats {

    private class GsonCompatibleIndexSizes {
        @SerializedName("_id_")
        private int _id_;
        public GsonCompatibleIndexSizes(int _id_) {
            this._id_ = _id_;
        }
        public int getDate() { return _id_; }
        public void setDate(int _id_) { this._id_ = _id_; }
    }

    private class GsonCompatibleIndexDetails {
        public GsonCompatibleIndexDetails() {}
    }

    public String ns;
    public int count;
    public int size;
    public int avgObjSize;
    public int numExtents;
    public int storageSize;
    public int lastExtentSize;
    public int paddingFactor;
    public String paddingFactorNote;
    public int userFlags;
    public boolean capped;
    public int nindexes;
    public GsonCompatibleIndexDetails indexDetails;
    public int totalIndexSize;
    GsonCompatibleIndexSizes indexSizes;
    public int ok;
}
