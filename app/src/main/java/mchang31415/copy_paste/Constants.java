package mchang31415.copy_paste;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public interface Constants {
    boolean DEBUG = true;
    int SECOND = 1000;

    String DATABASE_NAME = "dbName",
           COLLECTION_NAME= "dbHost",
           API_KEY = "dbPort";

    String mongoConfigFilename = "copy_paste_mongo_config";
    int MAXCOLLECTIONSIZE = 5242880;
    String MLAB_BASEURL = "https://api.mlab.com/api/1";

    int CONNECT_TIMEOUT = 15 * SECOND;
    int READ_TIMEOUT = 10 * SECOND;
    int CAPPED_SIZE = 20971520;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
}
