package mchang31415.copy_paste;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtility {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public enum NetworkStatus {
        NOT_CONNECTED, WIFI, MOBILE
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static NetworkStatus getConnectivityStatusString(Context context) {
        int conn = NetworkUtility.getConnectivityStatus(context);
        if (conn == NetworkUtility.TYPE_WIFI)
            return NetworkStatus.WIFI;
        if (conn == NetworkUtility.TYPE_MOBILE)
            return NetworkStatus.MOBILE;
        return NetworkStatus.NOT_CONNECTED;
    }
}
