package mchang31415.copy_paste;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.JsonSyntaxException;

public class mlabAPIWrapper implements Constants {
    private static final String TAG = mlabAPIWrapper.class.getSimpleName();

    private String mApiKey;
    private Map<String, String> keyMap;
    private Gson mGson;
    private Type listStringType, listMlabEntryType;

    mlabAPIWrapper(String apiKey) {
        mGson = new Gson();
        mApiKey = apiKey;
        keyMap = new HashMap<>();
        keyMap.put("apiKey", mApiKey);
        //Gson doesn't have a way to format generic types.
        //Use TypeToken as a medium to get type of List<String>
        listStringType = new TypeToken<List<String>>(){}.getType();
        listMlabEntryType = new TypeToken<List<mlabEntry>>(){}.getType();
    }

    private void LogNetworkException(String functionName,
                                     String exceptionDescription,
                                     String url,
                                     String params,
                                     Exception e)
    {
        Log.d(TAG, functionName + " " + exceptionDescription +
                "\nURL: " + url +
                "\nparams/data: " + params, e);
    }

    public List<String> listDatabases() {
        String resourceURL = MLAB_BASEURL + "/databases";
        Map<String, String> params = new HashMap<>(keyMap);
        try {
            String response = HTTPManager.httpGetRequest(resourceURL, params);
            return mGson.fromJson(response, listStringType);
        } catch (IOException e) {
            LogNetworkException("listDatabases()", "IOException from get request.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        } catch (JsonSyntaxException e) {
            LogNetworkException("listDatabases()", "JsonSyntaxException from gson parser.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        }
    }

    public List<String> listDatabases(String cluster) {
        String resourceURL = MLAB_BASEURL + "/clusters/" + cluster + "/databases";
        Map<String, String> params = new HashMap<>(keyMap);
        try {
            String response = HTTPManager.httpGetRequest(resourceURL, params);
            return mGson.fromJson(response, listStringType);
        } catch (IOException e) {
            LogNetworkException("listDatabases(cluster)", "IOException from get request.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        } catch (JsonSyntaxException e) {
            LogNetworkException("listDatabases(cluster)", "JsonSyntaxException from gson parser.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        }
    }

    public List<String> listCollections(String database) {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections";
        Map<String, String> params = new HashMap<>(keyMap);
        try {
            String response = HTTPManager.httpGetRequest(resourceURL, params);
            return mGson.fromJson(response, listStringType);
        } catch (IOException e) {
            LogNetworkException("listCollections()", "IOException from get request.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        } catch (JsonSyntaxException e) {
            LogNetworkException("listCollections()", "JsonSyntaxException from gson parser.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        }
    }

    // Optional parameters:
    // q  = <query>---------------restrict results by JSON query
    // c  = true------------------return result count for query
    // f  = <set of fields>-------specify which fields to include or exclude from each document (1/0 = in/ex)
    // s  = <sort order>----------specify the order in which to sort each field (1/-1 = asc/des)
    // sk = <num results to skip>-number of results to skip in result set
    // l  = <limit>---------------specify limit for number of results (default 1000)
    public List<mlabEntry> listDocuments(String database,
                                         String collection,
                                         Map<String, String> params)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        params.putAll(keyMap);
        try {
            String response = HTTPManager.httpGetRequest(resourceURL, params);
            return mGson.fromJson(response, listMlabEntryType);
        } catch (IOException e) {
            LogNetworkException("listDocuments()", "IOException from get request.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        } catch (JsonSyntaxException e) {
            LogNetworkException("listDocuments()", "JsonSyntaxException from gson parser.",
                    resourceURL, params.toString(), e);
            return Collections.emptyList();
        }
    }

    // List all documents. No query parameters.
    public List<mlabEntry> listDocuments(String database, String collection) {
        return listDocuments(database, collection, new HashMap<String, String>());
    }

    public mlabEntry getMostRecentDocument(String database,
                                           String collection)
    {
        Map<String, String> params = new HashMap<>();
        params.put("s", "{\"$natural\": -1}");
        params.put("l", "1");
        List<mlabEntry> list = listDocuments(database, collection, params);
        if (!list.isEmpty()) return list.get(0);
        return new mlabEntry();
    }

    public boolean insertDocuments(String database,
                                   String collection,
                                   List<mlabEntry> documents)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        resourceURL += "?" + HTTPManager.formatQuery(keyMap);
        String data = "[ ";
        for(mlabEntry entry : documents)
            data += entry.stringify();
        data += " ]";
        try {
            HTTPManager.httpPostRequest(resourceURL, data);
            return true;
        } catch (IOException e) {
            LogNetworkException("insertDocuments()", "IOException from post request.",
                    resourceURL, data, e);
            return false;
        }
    }

    public String insertDocument(String database,
                                  String collection,
                                  mlabEntry document)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        resourceURL += "?" + HTTPManager.formatQuery(keyMap);
        String data = document.stringify();
        String response = "";
        try {
            response = HTTPManager.httpPostRequest(resourceURL, data);
        } catch(IOException e) {
            LogNetworkException("insertDocument()", "IOException from post request.",
                    resourceURL, data, e);
        }
        return response;
    }

    public boolean updateMultipleDocuments(String database,
                                           String collection,
                                           Map<String, String> params,
                                           String replacementDocument)
    {
        //not written
        return true;
    }

    public boolean replaceMultipleDocuments(String database,
                                            String collection,
                                            Map<String, String> params,
                                            mlabEntry replacementDocument)
    {
        //not written
        return true;
    }

    public boolean deleteMultipleDocuments(String database,
                                           String collection,
                                           Map<String, String> params)
    {
        //not written
        return true;
    }

    public mlabEntry getDocumentWithId(String database,
                                       String collection,
                                       String id)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        resourceURL += "/" + id;
        Map<String, String> params = new HashMap<>(keyMap);
        try {
            String response = HTTPManager.httpGetRequest(resourceURL, params);
            return mGson.fromJson(response, mlabEntry.class);
        } catch (IOException e) {
            LogNetworkException("getDocumentWithId()", "IOException from get request.",
                    resourceURL, params.toString(), e);
            return new mlabEntry();
        } catch (JsonSyntaxException e) {
            LogNetworkException("getDocumentWithId()", "JsonSyntaxException from gson parser.",
                    resourceURL, params.toString(), e);
            return new mlabEntry();
        }
    }

    public boolean updateDocumentWithId(String database,
                                        String collection,
                                        String id,
                                        mlabEntry replacementDocument)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        resourceURL += "/" + id + "?" + HTTPManager.formatQuery(keyMap);
        String replacementData = String.format("{ \"$set\" : %s }", replacementDocument.stringify());
        try {
            HTTPManager.httpPutRequest(resourceURL, replacementData);
        } catch (IOException e) {
            LogNetworkException("updateDocumentWithId()", "IOException from put request.",
                    resourceURL, replacementData, e);
            return false;
        }
        return true;
    }

    public boolean deleteDocumentWithId(String database,
                                        String collection,
                                        String id)
    {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/collections/" + collection;
        resourceURL += "/" + id + "?" + HTTPManager.formatQuery(keyMap);
        try {
            HTTPManager.httpDeleteRequest(resourceURL);
        } catch (IOException e) {
            LogNetworkException("deleteDocumentWithId()", "IOException from delete request.",
                    resourceURL, "", e);
            return false;
        }
        return true;
    }

    public enum mlabCommand {
        COLLSTATS, CONVERTOCAPPED
    };

    public Object runMlabCommand(String database,
                                 String collection,
                                 mlabCommand command) {
        String resourceURL = MLAB_BASEURL + "/databases/" + database + "/runCommand";
        resourceURL += "?" + HTTPManager.formatQuery(keyMap);
        if (command == mlabCommand.CONVERTOCAPPED) {
            JSONObject data = new JSONObject();
            try {
                data.put("convertToCapped", collection);
                data.put("size", CAPPED_SIZE);
                HTTPManager.httpPostRequest(resourceURL, data.toString());
                return true;
            } catch (Exception e) {
                LogNetworkException("runMlabCommand()", "convertToCapped error (post request).",
                        resourceURL, data.toString(), e);
                return false;
            }
        }
        if (command == mlabCommand.COLLSTATS) {
            JSONObject data = new JSONObject();
            try {
                data.put("collStats", collection);
                String response = HTTPManager.httpPostRequest(resourceURL, data.toString());
                return mGson.fromJson(response, CollectionStats.class);
            } catch (IOException e) {
                LogNetworkException("runMlabCommand()", "collStats error IOException (post request).",
                        resourceURL, data.toString(), e);
                return new CollectionStats();
            } catch (JSONException e) {
                LogNetworkException("runMlabCommand()", "collStats error JSONException.",
                        resourceURL, data.toString(), e);
                return new CollectionStats();
            }
            catch (JsonSyntaxException e) {
                LogNetworkException("runMlabCommand()", "collStats error JsonSyntaxException from gson parser.",
                        resourceURL, data.toString(), e);
                return new CollectionStats();
            }

        }
        return null;
    }
}
