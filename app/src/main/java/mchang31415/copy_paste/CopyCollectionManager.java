package mchang31415.copy_paste;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.*;

class CopyCollectionManager implements Constants {
    private static final String TAG = CopyCollectionManager.class.getSimpleName();

    private String database, collection;
    private mlabAPIWrapper mlabAPI;
    private String latestObjectId;
    private Gson m_Gson;

    private mlabEntry tryParse(String s) {
        try {
            return m_Gson.fromJson(s, mlabEntry.class);
        } catch (JsonSyntaxException e) {
            Log.d(TAG, "JsonSyntaxException from \n" + s, e);
        }
        return new mlabEntry();
    }

    CopyCollectionManager(String database, String collection, String key) {
        this.database = database;
        this.collection = collection;
        m_Gson = new Gson();
        mlabAPI = new mlabAPIWrapper(key);
    }

    //init contains networking
    public void init() {
        //Get latest Document. Query in natural order since the collection should be capped
        Map<String, String> params = new HashMap<>();
        params.put("s", "{\"$natural\":-1}");
        List<mlabEntry> documents = mlabAPI.listDocuments(database, collection, params);
        //If there are no documents, insert a dummy first document.
        if (documents.isEmpty()) {
            mlabEntry dummyDocument = new mlabEntry();
            dummyDocument.setValue("dummy");
            dummyDocument.setTimestamp();
            //Inserting a document will return the new document with the object id.
            String response = mlabAPI.insertDocument(database, collection, dummyDocument);
            latestObjectId = tryParse(response).getObjectId();
        } else {
            latestObjectId = documents.get(0).getObjectId();
        }
    }

    boolean insertEntry(String value) {
        //only insert entry if it differs from the latest entry
        String latestEntry = getLatestEntryValue();
        if (latestEntry.equals(value)) return false;
        mlabEntry newEntry = new mlabEntry(latestObjectId, value);
        String response = mlabAPI.insertDocument(database, collection, newEntry);
        latestObjectId = tryParse(response).getObjectId();
        return true;
    }

    private String getLatestEntryValue() {
        mlabEntry latestEntry = mlabAPI.getMostRecentDocument(database, collection);
        return latestEntry.getValue();
    }
}
