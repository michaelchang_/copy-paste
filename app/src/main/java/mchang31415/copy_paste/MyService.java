package mchang31415.copy_paste;

import android.app.Notification;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;

import java.util.concurrent.Executor;

public class MyService extends Service implements Constants {

    private ClipboardManager m_clipboardManager;
    private Looper m_serviceLooper;
    private ServiceHandler m_serviceHandler;

    @Override
    public void onCreate() {
        m_clipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);

        HandlerThread thread = new HandlerThread("MyService", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        m_serviceLooper = thread.getLooper();
    }

    @Override
    public void onDestroy() {
        //kill thread
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String database   = intent.getStringExtra(DATABASE_NAME);
        final String collection = intent.getStringExtra(COLLECTION_NAME);
        final String apiKey     = intent.getStringExtra(API_KEY);
        m_serviceHandler = new ServiceHandler(m_serviceLooper, database, collection, apiKey);
        m_clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
            @Override
            public void onPrimaryClipChanged() {
                final String clipboardText = readClipboard();
                Message clipboardTextMessage = m_serviceHandler.obtainMessage();
                clipboardTextMessage.obj = clipboardText;
                m_serviceHandler.sendMessage(clipboardTextMessage);
            } // onPrimaryClipChanged
        }); //addPrimaryClipChangedListener

        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String readClipboard() {
        ClipData clip = m_clipboardManager.getPrimaryClip();
        if (clip != null) {
            ClipData.Item item = clip.getItemAt(0);
            return item.coerceToText(MyService.this.getApplicationContext()).toString();
        }
        return "";
    }

    private final class ServiceHandler extends Handler {
        CopyCollectionManager m_copyCollectionManager;
        public ServiceHandler(Looper looper,
                              String database,
                              String collection,
                              String apiKey)
        {
            super(looper);
            m_copyCollectionManager = new CopyCollectionManager(database, collection, apiKey);
        }
        @Override
        public void handleMessage(Message message) {
            m_copyCollectionManager.init();
            String clipboardText = (String)message.obj;
            m_copyCollectionManager.insertEntry(clipboardText);
        }
    } // ServiceHandler class

}
