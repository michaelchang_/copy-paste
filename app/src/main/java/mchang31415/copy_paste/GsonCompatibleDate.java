package mchang31415.copy_paste;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.text.ParseException;
import android.util.Log;

public class GsonCompatibleDate implements Constants {

    private static final String TAG = GsonCompatibleDate.class.getSimpleName();

    @SerializedName("$date")
    private Date m_date;
    public GsonCompatibleDate(String date) {
        try {
            m_date = dateFormat.parse(date);
        } catch (ParseException e) {
            Log.d(TAG, "GsonCompatibleDate() Unable to parse.");
        }
    }
    public GsonCompatibleDate(Date date) {
        m_date = date;
    }
    public Date getDate() { return m_date; }
    public void setDate(Date date) { m_date = date;}
}
