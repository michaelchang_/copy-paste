package mchang31415.copy_paste;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import java.io.Closeable;

final class HTTPManager implements Constants {
    private static final String TAG = HTTPManager.class.getSimpleName();
    private static String charSet = StandardCharsets.UTF_8.name();

    private static boolean cleanupStream(Closeable stream, String printOnFailure) {
        if (stream == null) {
            Log.d(TAG, printOnFailure);
            return false;
        }
        try {
            stream.close();
            return true;
        } catch (Exception e) {
            Log.d(TAG, printOnFailure);
            return false;
        }
    }

    static String formatQuery(Map<String, String> params) {
        String query = "";
        boolean first = true;
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (!first) query += "&";
                query += entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), charSet);
                first = false;
            }

        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "formatQuery() Encoding is unsupported.", e);
        }
        return query;
    }

    static String httpGetRequest(String url, Map<String, String> params) throws IOException {
        String query = formatQuery(params);
        HttpURLConnection connection = (HttpURLConnection)new URL(url + "?" + query).openConnection(Proxy.NO_PROXY);
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setRequestProperty("Accept-Charset", charSet);
        InputStream inputStream = connection.getInputStream();

        String responseBody = new Scanner(inputStream).useDelimiter("\\A").next();
        inputStream.close();
        connection.disconnect();
        return responseBody;

    }

    private static String httpPostOrPutRequest(String url, String data, String requestMethod) throws IOException {
        Log.d(TAG, url);
        //encodes the data into a byte array to write to output stream
        byte[] postData = data.getBytes(charSet);

        HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true); //set POST
        connection.setRequestMethod(requestMethod);
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setRequestProperty("Accept-Charset", charSet);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
        connection.setUseCaches(false);

        DataOutputStream outputStream = null;
        InputStream inputStream = null;
        String responseBody = "";

        try {
            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.write(postData);
            outputStream.flush();

            inputStream = connection.getInputStream();
            responseBody = new Scanner(inputStream).useDelimiter("\\A").next();
        } catch(Exception e) {
            Log.d(TAG, "httpPostOrPutRequest() outputStream/inputStream IOException", e);
        } finally {
            cleanupStream(outputStream, "output stream close");
            cleanupStream(inputStream, "input stream close");
        }
        connection.disconnect();
        return responseBody;
    }

    static String httpPostRequest(String url, String data) throws IOException {
        return httpPostOrPutRequest(url, data, "POST");
    }
    static String httpPutRequest(String url, String data) throws IOException {
        return httpPostOrPutRequest(url, data, "PUT");
    }

    static int httpDeleteRequest(String url) throws IOException {
        Log.d(TAG, url);
        HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
        connection.setDoOutput(true); //set POST
        connection.setRequestMethod("DELETE");
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setUseCaches(false);

        int response = 0;
        try {
            connection.connect();
            response = connection.getResponseCode();
        } catch(Exception e) {
            Log.d(TAG, "httpDeleteRequest() Connection error.", e);
        }
        connection.disconnect();
        return response;
    }

    public static Map<String, List<String>> httpResponseHeader(URLConnection connection) {
        Map<String, List<String>> header = new HashMap<>();
        for(Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
            header.put(entry.getKey(), entry.getValue());
        }
        return header;
    }

    public static int httpResponseCode (URLConnection connection) throws IOException {
        return ((HttpURLConnection)connection).getResponseCode();
    }

}

